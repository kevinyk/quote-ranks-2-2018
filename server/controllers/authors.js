var mongoose = require('mongoose');
var Author = mongoose.model('Author');
module.exports = {
    create: function(req,res){
        var anAuthor = new Author(req.body);
        anAuthor.save(function(err){
            if(err){
                console.log('validation errors');
                res.json(err);
            }else{
                res.json(anAuthor);
            }
        })
    },
    index: function(req,res){
        Author.find().exec(function(err, foundAuthors){
            res.json(foundAuthors);
        })
    },
    show: function(req,res){
        Author.findOne({_id: req.params.id}).populate('quotes').exec(function(err, foundAuthor){
            res.json(foundAuthor);
        })
    }
}