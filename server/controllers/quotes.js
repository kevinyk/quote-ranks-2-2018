var mongoose = require('mongoose');
// WHOOPS
var Quote = mongoose.model('Quote');
var Author = mongoose.model('Author');
module.exports = {
	index: function(req,res){
		Quote.find().exec(function(err, foundQuotes){
			if(err){
				res.json(err);
			}else{
				res.json(foundQuotes);
			}
		})
	},
	create: function(req,res){
		Author.findOne({_id:req.params.id}, function(err, foundAuthor){
			var aNewQuote = new Quote(req.body);
			aNewQuote.save(function(err){
				if(err){
					res.json(err);
				}else{
					foundAuthor.quotes.push(aNewQuote);
					foundAuthor.save(function(err){

						res.json(foundAuthor);
					})
				}
			})
		})
	},
	voteUp: function(req,res){
		Quote.update({_id: req.params.id}, {$inc:{score:1}}, function(err){
			if(err){
				console.log('something went wrong');
				res.json(err);
			}else{
				res.json(true);
			}
		})
	},
	voteDown: function(req,res){
		Quote.update({_id: req.params.id}, {$inc:{score:-1}}, function(err){
			if(err){
				console.log('something went wrong');
				res.json(err);
			}else{
				res.json(true);
			}
		})
	},
	delete: function(req,res){
		Quote.remove({_id: req.params.id}, function(err){
			if(err){
				console.log('failed to delete quote');
				res.json(err);
			}else{
				res.json(true);
			}
		})
	}

}