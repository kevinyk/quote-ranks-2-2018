var Quotes = require('./../controllers/quotes.js');
var Authors = require('./../controllers/authors.js');
module.exports = function(app){
	app.get('/api/quotes', Quotes.index);
	app.post('/api/authors/:id/quotes', Quotes.create);
	app.get('/api/quotes/upvote/:id', Quotes.voteUp);
	app.get('/api/quotes/downvote/:id', Quotes.voteDown);
	app.get('/api/quotes/delete/:id', Quotes.delete);
	app.post('/api/authors', Authors.create);
	app.get('/api/authors', Authors.index);
	app.get('/api/authors/:id/quotes', Authors.show);
}
