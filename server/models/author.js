var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var AuthorSchema = new mongoose.Schema({
	name:{type:String, minlength: 4},
	quotes:[{type: Schema.Types.ObjectId, ref: 'Quote'}],
}, {timestamps:true}, {usePushEach: true})

mongoose.model('Author', AuthorSchema);