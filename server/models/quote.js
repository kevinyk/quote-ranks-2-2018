var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var QuoteSchema = new mongoose.Schema({
	content:{type:String, minlength: 4},
	score: {type: Number, default: 0}
}, {timestamps:true})

mongoose.model('Quote', QuoteSchema);