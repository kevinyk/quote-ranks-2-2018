import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuotesComponent } from './quotes/quotes.component';
import { AuthorsNewComponent } from './authors-new/authors-new.component';
import { AuthorsShowComponent } from './authors-show/authors-show.component';
import { QuotesNewComponent } from './quotes-new/quotes-new.component';
const routes: Routes = [
    {
        path: '',
        component: QuotesComponent
    },
    {
        path: 'new',
        component: AuthorsNewComponent
    },
    {
        path: 'quotes/:author_id',
        component: AuthorsShowComponent
    },
    {
        path: 'write/:author_id',
        component: QuotesNewComponent
    }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }