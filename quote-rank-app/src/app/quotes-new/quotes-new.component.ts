import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Author } from './../author';
import { Quote } from './../quote';

@Component({
  selector: 'app-quotes-new',
  templateUrl: './quotes-new.component.html',
  styleUrls: ['./quotes-new.component.css']
})
export class QuotesNewComponent implements OnInit {
  author: Author = new Author();
  newQuote: Quote = new Quote();
  constructor(private _route: ActivatedRoute, private _apiService: ApiService, private _router: Router) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params) => {
      var authorId = params.get('author_id');
      this._apiService.getAuthorWithQuotes(authorId)
        .subscribe((responseData: Author) => {
          this.author = responseData;
        })
    })
  }
  submitQuote() {
    console.log('submitQuote');
    this._apiService.postQuote(this.newQuote, this.author._id)
      .subscribe((responseData: Quote) => {
        console.log('responseData', responseData);
        this.newQuote = new Quote();
        this._router.navigate(['/quotes', this.author._id]);
      })
  }

}
