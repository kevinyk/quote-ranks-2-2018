import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-authors-new',
  templateUrl: './authors-new.component.html',
  styleUrls: ['./authors-new.component.css']
})
export class AuthorsNewComponent implements OnInit {
  newAuthor: object = {name: ''};
  constructor(private _apiService:ApiService, private _router:Router) { }

  ngOnInit() {
  }
  submitAuthor(){
    this._apiService.postAuthor(this.newAuthor)
    .subscribe((responseData: any)=>{
      console.log(responseData);
      this._router.navigate(['/quotes', responseData._id]);
    })
  }

}
