import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {
  constructor(private _http: HttpClient) { }
  postQuote(quote, authorId){
  	return this._http.post(`/api/authors/${authorId}/quotes`, quote)
  }
  getQuotes(){
  	return this._http.get('/api/quotes')
  }
  voteUp(quoteId){
  	return this._http.get(`/api/quotes/upvote/${quoteId}`);
  }
  voteDown(quoteId){
  	return this._http.get(`/api/quotes/downvote/${quoteId}`);
  }
  deleteQuote(quoteId){
    return this._http.get(`/api/quotes/delete/${quoteId}`);
  }
  postAuthor(authorObj){
    return this._http.post('/api/authors', authorObj);
  }
  getAuthors(){
    return this._http.get('/api/authors');
  }
  getAuthorWithQuotes(authorId){
    return this._http.get(`/api/authors/${authorId}/quotes`);
  }
}
