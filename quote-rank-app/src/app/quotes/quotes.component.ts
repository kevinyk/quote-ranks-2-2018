import { Component, OnInit } from '@angular/core';
import { Quote } from './../quote';
import { ApiService } from './../api.service';
import { Author } from './../author';
@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  newQuote: Quote = new Quote();
	allQuotes: Quote[]=[];
	authors: Author[] = [];
  constructor(private _apiService: ApiService) { }

  ngOnInit() {
		this.getAllQuotes();
		this.getAllAuthors();
	}
	getAllAuthors(){
		console.log('getAllAuthors');
		this._apiService.getAuthors()
		.subscribe((responseData:Author[])=>{
			this.authors = responseData;
		})
	}
  getAllQuotes(){
  	console.log('getAllQuotes');
  	this._apiService.getQuotes()
  	.subscribe((responseData: Quote[])=>{
  		console.log('responseData', responseData);
  		this.allQuotes = responseData;
  	})
  }
  voteUp(quoteId){
  	this._apiService.voteUp(quoteId)
  	.subscribe((responseData: any)=>{
  		console.log('responseData', responseData);
  		this.getAllQuotes();
  	})
  }
  voteDown(quoteId){
  	this._apiService.voteDown(quoteId)
  	.subscribe((responseData: any)=>{
  		console.log('responseData', responseData);
  		this.getAllQuotes();
  	})
  }
  deleteQuote(quoteId){
    this._apiService.deleteQuote(quoteId)
    .subscribe((responseData: any)=>{
      console.log(responseData);
      this.getAllQuotes();
    })
  }
}
