import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Author } from './../author';

@Component({
  selector: 'app-authors-show',
  templateUrl: './authors-show.component.html',
  styleUrls: ['./authors-show.component.css']
})
export class AuthorsShowComponent implements OnInit {
  author: Author = new Author();
  constructor(private _apiService: ApiService, private _router:Router, private _route: ActivatedRoute) { }

  ngOnInit() {
    this.getAllQuotes();
  }
  getAllQuotes(){
    this._route.paramMap.subscribe((params) => {
      var authorId = params.get('author_id');
      this._apiService.getAuthorWithQuotes(authorId)
        .subscribe((responseData: Author) => {
          this.author = responseData;
        })
    })
  }
  voteUp(quoteId) {
    this._apiService.voteUp(quoteId)
      .subscribe((responseData: any) => {
        console.log('responseData', responseData);
        this.getAllQuotes();
      })
  }
  voteDown(quoteId) {
    this._apiService.voteDown(quoteId)
      .subscribe((responseData: any) => {
        console.log('responseData', responseData);
        this.getAllQuotes();
      })
  }
  deleteQuote(quoteId) {
    this._apiService.deleteQuote(quoteId)
      .subscribe((responseData: any) => {
        console.log(responseData);
        this.getAllQuotes();
      })
  }

}
