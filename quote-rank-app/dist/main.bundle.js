webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/api.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var ApiService = /** @class */ (function () {
    function ApiService(_http) {
        this._http = _http;
    }
    ApiService.prototype.postQuote = function (quote, authorId) {
        return this._http.post("/api/authors/" + authorId + "/quotes", quote);
    };
    ApiService.prototype.getQuotes = function () {
        return this._http.get('/api/quotes');
    };
    ApiService.prototype.voteUp = function (quoteId) {
        return this._http.get("/api/quotes/upvote/" + quoteId);
    };
    ApiService.prototype.voteDown = function (quoteId) {
        return this._http.get("/api/quotes/downvote/" + quoteId);
    };
    ApiService.prototype.deleteQuote = function (quoteId) {
        return this._http.get("/api/quotes/delete/" + quoteId);
    };
    ApiService.prototype.postAuthor = function (authorObj) {
        return this._http.post('/api/authors', authorObj);
    };
    ApiService.prototype.getAuthors = function () {
        return this._http.get('/api/authors');
    };
    ApiService.prototype.getAuthorWithQuotes = function (authorId) {
        return this._http.get("/api/authors/" + authorId + "/quotes");
    };
    ApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;


/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var quotes_component_1 = __webpack_require__("../../../../../src/app/quotes/quotes.component.ts");
var authors_new_component_1 = __webpack_require__("../../../../../src/app/authors-new/authors-new.component.ts");
var authors_show_component_1 = __webpack_require__("../../../../../src/app/authors-show/authors-show.component.ts");
var quotes_new_component_1 = __webpack_require__("../../../../../src/app/quotes-new/quotes-new.component.ts");
var routes = [
    {
        path: '',
        component: quotes_component_1.QuotesComponent
    },
    {
        path: 'new',
        component: authors_new_component_1.AuthorsNewComponent
    },
    {
        path: 'quotes/:author_id',
        component: authors_show_component_1.AuthorsShowComponent
    },
    {
        path: 'write/:author_id',
        component: quotes_new_component_1.QuotesNewComponent
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Quote Ranks Plus</h1>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var quotes_component_1 = __webpack_require__("../../../../../src/app/quotes/quotes.component.ts");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var authors_new_component_1 = __webpack_require__("../../../../../src/app/authors-new/authors-new.component.ts");
var authors_show_component_1 = __webpack_require__("../../../../../src/app/authors-show/authors-show.component.ts");
var quotes_new_component_1 = __webpack_require__("../../../../../src/app/quotes-new/quotes-new.component.ts");
var app_routing_module_1 = __webpack_require__("../../../../../src/app/app-routing.module.ts");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                quotes_component_1.QuotesComponent,
                authors_new_component_1.AuthorsNewComponent,
                authors_show_component_1.AuthorsShowComponent,
                quotes_new_component_1.QuotesNewComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                app_routing_module_1.AppRoutingModule
            ],
            providers: [api_service_1.ApiService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/author.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Author = /** @class */ (function () {
    function Author(name, _id) {
        if (name === void 0) { name = ''; }
        if (_id === void 0) { _id = ''; }
        this.name = name;
        this._id = _id;
    }
    return Author;
}());
exports.Author = Author;


/***/ }),

/***/ "../../../../../src/app/authors-new/authors-new.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/authors-new/authors-new.component.html":
/***/ (function(module, exports) {

module.exports = "<a [routerLink]=\"['/']\">Home</a>\n<p>Add a new quotable author</p>\n<fieldset>\n  <form (submit)=\"submitAuthor()\">\n    <input type=\"text\" [(ngModel)]=\"newAuthor.name\" name=\"name\">\n    <input type=\"submit\">\n  </form>\n</fieldset>"

/***/ }),

/***/ "../../../../../src/app/authors-new/authors-new.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var AuthorsNewComponent = /** @class */ (function () {
    function AuthorsNewComponent(_apiService, _router) {
        this._apiService = _apiService;
        this._router = _router;
        this.newAuthor = { name: '' };
    }
    AuthorsNewComponent.prototype.ngOnInit = function () {
    };
    AuthorsNewComponent.prototype.submitAuthor = function () {
        var _this = this;
        this._apiService.postAuthor(this.newAuthor)
            .subscribe(function (responseData) {
            console.log(responseData);
            _this._router.navigate(['/quotes', responseData._id]);
        });
    };
    AuthorsNewComponent = __decorate([
        core_1.Component({
            selector: 'app-authors-new',
            template: __webpack_require__("../../../../../src/app/authors-new/authors-new.component.html"),
            styles: [__webpack_require__("../../../../../src/app/authors-new/authors-new.component.css")]
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService, router_1.Router])
    ], AuthorsNewComponent);
    return AuthorsNewComponent;
}());
exports.AuthorsNewComponent = AuthorsNewComponent;


/***/ }),

/***/ "../../../../../src/app/authors-show/authors-show.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/authors-show/authors-show.component.html":
/***/ (function(module, exports) {

module.exports = "<a [routerLink]=\"['/']\">Home</a> | <a [routerLink]=\"['/write', author._id]\">Add a quote</a>\n<p>Quotes by {{author.name}}</p>\n\n<table>\n  <thead>\n    <th>Quote</th>\n    <th>Votes</th>\n    <th>Actions Available</th>\n  </thead>\n  <tr *ngFor=\"let quote of author.quotes\">\n    <td>{{quote.content}}</td>\n    <td>{{quote.score}}</td>\n    <td><button (click)=\"voteUp(quote._id)\">Vote Up</button> <button (click)=\"voteDown(quote._id)\">Vote down</button> <button (click)=\"deleteQuote(quote._id)\">Delete</button></td>\n  </tr>\n</table>"

/***/ }),

/***/ "../../../../../src/app/authors-show/authors-show.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var router_2 = __webpack_require__("../../../router/esm5/router.js");
var author_1 = __webpack_require__("../../../../../src/app/author.ts");
var AuthorsShowComponent = /** @class */ (function () {
    function AuthorsShowComponent(_apiService, _router, _route) {
        this._apiService = _apiService;
        this._router = _router;
        this._route = _route;
        this.author = new author_1.Author();
    }
    AuthorsShowComponent.prototype.ngOnInit = function () {
        this.getAllQuotes();
    };
    AuthorsShowComponent.prototype.getAllQuotes = function () {
        var _this = this;
        this._route.paramMap.subscribe(function (params) {
            var authorId = params.get('author_id');
            _this._apiService.getAuthorWithQuotes(authorId)
                .subscribe(function (responseData) {
                _this.author = responseData;
            });
        });
    };
    AuthorsShowComponent.prototype.voteUp = function (quoteId) {
        var _this = this;
        this._apiService.voteUp(quoteId)
            .subscribe(function (responseData) {
            console.log('responseData', responseData);
            _this.getAllQuotes();
        });
    };
    AuthorsShowComponent.prototype.voteDown = function (quoteId) {
        var _this = this;
        this._apiService.voteDown(quoteId)
            .subscribe(function (responseData) {
            console.log('responseData', responseData);
            _this.getAllQuotes();
        });
    };
    AuthorsShowComponent.prototype.deleteQuote = function (quoteId) {
        var _this = this;
        this._apiService.deleteQuote(quoteId)
            .subscribe(function (responseData) {
            console.log(responseData);
            _this.getAllQuotes();
        });
    };
    AuthorsShowComponent = __decorate([
        core_1.Component({
            selector: 'app-authors-show',
            template: __webpack_require__("../../../../../src/app/authors-show/authors-show.component.html"),
            styles: [__webpack_require__("../../../../../src/app/authors-show/authors-show.component.css")]
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService, router_1.Router, router_2.ActivatedRoute])
    ], AuthorsShowComponent);
    return AuthorsShowComponent;
}());
exports.AuthorsShowComponent = AuthorsShowComponent;


/***/ }),

/***/ "../../../../../src/app/quote.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Quote = /** @class */ (function () {
    function Quote(content, author) {
        if (content === void 0) { content = ""; }
        if (author === void 0) { author = ""; }
        this.content = content;
        this.author = author;
    }
    return Quote;
}());
exports.Quote = Quote;


/***/ }),

/***/ "../../../../../src/app/quotes-new/quotes-new.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/quotes-new/quotes-new.component.html":
/***/ (function(module, exports) {

module.exports = "<a [routerLink]=\"['/']\">Home</a>\n\n<p>Provide a quote by {{author.name}}</p>\n<fieldset>\n  <p>Quote:</p>\n  <form (submit)=\"submitQuote()\">\n    <input type=\"text\" name=\"content\" [(ngModel)]=\"newQuote.content\">\n    <button [routerLink]=\"['/quotes', author._id]\">Cancel</button>\n    <input type=\"submit\">\n  </form>\n</fieldset>"

/***/ }),

/***/ "../../../../../src/app/quotes-new/quotes-new.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var author_1 = __webpack_require__("../../../../../src/app/author.ts");
var quote_1 = __webpack_require__("../../../../../src/app/quote.ts");
var QuotesNewComponent = /** @class */ (function () {
    function QuotesNewComponent(_route, _apiService, _router) {
        this._route = _route;
        this._apiService = _apiService;
        this._router = _router;
        this.author = new author_1.Author();
        this.newQuote = new quote_1.Quote();
    }
    QuotesNewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.paramMap.subscribe(function (params) {
            var authorId = params.get('author_id');
            _this._apiService.getAuthorWithQuotes(authorId)
                .subscribe(function (responseData) {
                _this.author = responseData;
            });
        });
    };
    QuotesNewComponent.prototype.submitQuote = function () {
        var _this = this;
        console.log('submitQuote');
        this._apiService.postQuote(this.newQuote, this.author._id)
            .subscribe(function (responseData) {
            console.log('responseData', responseData);
            _this.newQuote = new quote_1.Quote();
            _this._router.navigate(['/quotes', _this.author._id]);
        });
    };
    QuotesNewComponent = __decorate([
        core_1.Component({
            selector: 'app-quotes-new',
            template: __webpack_require__("../../../../../src/app/quotes-new/quotes-new.component.html"),
            styles: [__webpack_require__("../../../../../src/app/quotes-new/quotes-new.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute, api_service_1.ApiService, router_1.Router])
    ], QuotesNewComponent);
    return QuotesNewComponent;
}());
exports.QuotesNewComponent = QuotesNewComponent;


/***/ }),

/***/ "../../../../../src/app/quotes/quotes.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".quote, .score, .vote {\n\tdisplay: inline-block;\n\tvertical-align: top;\n}\n.quote {\n\twidth: 70%;\n}\n.score {\n\twidth: 10%;\n}\n.score, .vote {\n\tmargin-top: 15px;\n}\n.vote {\n\t/*text-align: right;*/\n}\n.vote button {\n\tmargin-left: auto;\n\tdisplay: block;\n\twidth:100%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/quotes/quotes.component.html":
/***/ (function(module, exports) {

module.exports = "<a [routerLink]=\"['/new']\">Add a quotable author</a>\n<p>We have quotes by:</p>\n<table>\n\t<thead>\n\t\t<th>Author</th>\n\t\t<th>Actions Available</th>\n\t</thead>\n\t<tr *ngFor=\"let author of authors\">\n\t\t<td>{{author.name}}</td>\n\t\t<td><button [routerLink]=\"['/quotes', author._id]\">View Quotes</button></td>\n\t</tr>\n</table>\n<!-- <h2>Add a quote</h2>\n<form  (submit)=\"submitQuote()\">\n\t<textarea name=\"content\" [(ngModel)]=\"newQuote.content\"></textarea>\n\t<p>Author <input type=\"text\" name=\"author\" [(ngModel)]=\"newQuote.author\"></p>\n\t<input type=\"submit\" value=\"Add Quote!\">\n</form>\n<h2>Quotes List</h2>\n<fieldset *ngFor=\"let quote of allQuotes\">\n\t<div class=\"quote\">\n\n\t\t<p>{{quote.content}}</p>\n\t\t<p>-{{quote.author}}</p>\n\t\t\n\t</div>\n\t<div class=\"score\">\n\t\t{{quote.score}}\n\t</div>\n\t<div class=\"vote\">\n\t\t<button (click)=\"voteUp(quote._id)\">Vote Up</button>\n\t\t<button (click)=\"voteDown(quote._id)\">Vote Down</button>\n\t\t<button (click)=\"deleteQuote(quote._id)\">Delete</button>\n\t</div>\n</fieldset> -->"

/***/ }),

/***/ "../../../../../src/app/quotes/quotes.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var quote_1 = __webpack_require__("../../../../../src/app/quote.ts");
var api_service_1 = __webpack_require__("../../../../../src/app/api.service.ts");
var QuotesComponent = /** @class */ (function () {
    function QuotesComponent(_apiService) {
        this._apiService = _apiService;
        this.newQuote = new quote_1.Quote();
        this.allQuotes = [];
        this.authors = [];
    }
    QuotesComponent.prototype.ngOnInit = function () {
        this.getAllQuotes();
        this.getAllAuthors();
    };
    QuotesComponent.prototype.getAllAuthors = function () {
        var _this = this;
        console.log('getAllAuthors');
        this._apiService.getAuthors()
            .subscribe(function (responseData) {
            _this.authors = responseData;
        });
    };
    QuotesComponent.prototype.getAllQuotes = function () {
        var _this = this;
        console.log('getAllQuotes');
        this._apiService.getQuotes()
            .subscribe(function (responseData) {
            console.log('responseData', responseData);
            _this.allQuotes = responseData;
        });
    };
    QuotesComponent.prototype.voteUp = function (quoteId) {
        var _this = this;
        this._apiService.voteUp(quoteId)
            .subscribe(function (responseData) {
            console.log('responseData', responseData);
            _this.getAllQuotes();
        });
    };
    QuotesComponent.prototype.voteDown = function (quoteId) {
        var _this = this;
        this._apiService.voteDown(quoteId)
            .subscribe(function (responseData) {
            console.log('responseData', responseData);
            _this.getAllQuotes();
        });
    };
    QuotesComponent.prototype.deleteQuote = function (quoteId) {
        var _this = this;
        this._apiService.deleteQuote(quoteId)
            .subscribe(function (responseData) {
            console.log(responseData);
            _this.getAllQuotes();
        });
    };
    QuotesComponent = __decorate([
        core_1.Component({
            selector: 'app-quotes',
            template: __webpack_require__("../../../../../src/app/quotes/quotes.component.html"),
            styles: [__webpack_require__("../../../../../src/app/quotes/quotes.component.css")]
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], QuotesComponent);
    return QuotesComponent;
}());
exports.QuotesComponent = QuotesComponent;


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map